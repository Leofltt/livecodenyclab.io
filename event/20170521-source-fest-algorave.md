---
name: Source Fest Algorave
venue: Brooklyn Bazaar
date: 2017-05-21 8:00 PM
image: https://scontent-lga3-2.xx.fbcdn.net/v/t31.0-8/17545520_1704380973195826_7396675897680197007_o.png?_nc_cat=110&ccb=2&_nc_sid=340051&_nc_ohc=tqlLV11ZvFMAX95vWWV&_nc_ht=scontent-lga3-2.xx&oh=c999647f3ba5e2f220da19f34c2f9579&oe=600424B7
address: 165 Banker St, Brooklyn, NY 11222
---

LiveCode.NYC presents
Source ALGORAVE
Sounds:
xname
Scorpion Mouse
2050
Parrises Squares
Visuals + Games:
Ulysses Popple
Ramsey Nasser + Tims Gardner
+MORE TBC
SourceFestival…
Join LiveCode.NYC and NYU's IDM for 3 days of performances, discussions, workshops, and of course and algorave, all centered around the practice of Live Coding. Music, visuals and even dance will be created with interactive systems as part of a festival celebrating coding in real time.
LiveCode.NYC...
We are a meet up group that focuses on the practice of live coding. We run several kinds of events. We like to say, "We are language agnostic and not medium specific. We live code games in LISP, music in Extempore, dance in Java, and everything in between. We program systems as they are running. Life changes in real-time and so does our code. Livecode.NYC.”