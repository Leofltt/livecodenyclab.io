---
name: Source Festival
venue: MAGNET at NYU Polytechnical School of Engineering
date: 2017-05-19 4:00 PM
address: 2 Metrotech, 8th Floor, Brooklyn, NY, 11201 Brooklyn, NY
image: https://scontent-lga3-2.xx.fbcdn.net/v/t31.0-8/17212025_1689561698011087_351859298594052978_o.jpg?_nc_cat=111&ccb=2&_nc_sid=340051&_nc_ohc=uRtaI4iCIvAAX-RkLbg&_nc_oc=AQlD7AtyL3OjEOPZCSeyEFZJ8F3ArRCNIklPdgzaKrb9KlEUar_QJq_lLSBXIlji2xo&_nc_ht=scontent-lga3-2.xx&oh=644914918613313c5b31fc115f2f80b6&oe=6002BE78
---

[TICKETS AVAILABLE HERE](https://www.eventbrite.co.uk/e/source-festival-tickets-33583397847?fbclid=IwAR1bn6MoMXj0rCNimX638EUthzf7UG5dL3uGuUEmK2pmAB_BMxbZmz9R_Iw)

FULL FESTIVAL TICKET INCLUDES ALGORAVE

Join LiveCode.NYC and NYU's IDM for 3 days of performances, discussions, workshops, and of course and algorave, all centered around the practice of Live Coding. Music, visuals and even dance will be created with interactive systems as part of a festival celebrating coding in real time.
Source Festival Program May 19-21st:

Friday 7pm - Concert Performances - MAGNET NYU
* Reckoner
* Daniel Steffy
* Vinton Surf
* Scorpion Mouse
* Bernardo Barros, Lester St. Louis and Zach Herchen

Saturday 3-6pm - Panel Discussions - MAGNET NYU
* Welcome to Live Coding
* Live coding and creating tools
* Live coding beyond sounds

Saturday 7pm - Concert Performances - MAGNET NYU
* cx_16
* Melody Loveless
* Avneesh Sarwate
* Yaxu (Live Stream)
* Norah Lorway
* Scott Carver

Sunday 3pm - Workshops - MAGNET
* Ulysses Popple - Exploring simple isomorphisms through visuals.
* Jason Levine - Livecoding x Machine Learning
* Scott Carver - Sound design patterns for SuperCollider

Sunday 8pm Doors - Algorave - BK Bazaar
* xname
* Scorpion Mouse
* 2050
* Parrises Squares
* Ramsey + Tims (gamez)
* Ulysses Popple (viz)

[https://www.facebook.com/events/423585617976951/](https://www.facebook.com/events/423585617976951/)