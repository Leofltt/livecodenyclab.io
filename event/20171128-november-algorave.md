---
name: November Algorave
venue: Sunnyvale
date: 2017-11-28 8:00 PM
image: https://scontent-lga3-2.xx.fbcdn.net/v/t31.0-8/22713383_1788913768075879_7824250213879535390_o.png?_nc_cat=100&ccb=2&_nc_sid=340051&_nc_ohc=FMlYmzB1JOEAX8mdBeu&_nc_ht=scontent-lga3-2.xx&oh=d14724453856eee339eec182fb837da3&oe=6003F379
address: 
---

A live coded evening of the succession of repetitive conditionals
==Featuring ==
Scorpio Mouse
2050
Codie (Sicchio x Sarah GHP)
Sean Lee
Ulysses Popple
==Tickets==
$10
==Algorave==
https://vimeo.com/228411462
==More Live Coding==
https://algorave.com/about/