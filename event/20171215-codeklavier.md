---
name: CodeKlavier
venue: MAGNET at NYU Polytechnic School of Engineering
date: 2017-12-15 7:00 PM
image: https://scontent-lga3-2.xx.fbcdn.net/v/t31.0-8/22713383_1788913768075879_7824250213879535390_o.png?_nc_cat=100&ccb=2&_nc_sid=340051&_nc_ohc=FMlYmzB1JOEAX8mdBeu&_nc_ht=scontent-lga3-2.xx&oh=d14724453856eee339eec182fb837da3&oe=6003F379
address: 
---

Programming with the piano as interface
https://www.youtube.com/watch?v=ytpB8FB6VTU

The CodeKlavier is a system created by Felipe Ignacio Noriega and Anne Veinberg which allows the pianist to live code through playing the piano. “Hello World” is the first prototype piece created especially for live control of the actions of a mechanical toy piano (The Robot Toy Piano) through playing the interface of an acoustic midi piano.

The system is built on Javascript and SuperCollider. It can be downloaded from https://narcode.github.io/codeklavier/. All software is open source and publicly available.