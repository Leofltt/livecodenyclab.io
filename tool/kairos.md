---
name: Kairos
image: https://raw.githubusercontent.com/Leofltt/Kairos/master/pics/kairosPic.png
link: https://github.com/Leofltt/Kairos
---

An [Haskell](https://www.haskell.org/) library for music composition and performance using [Csound](https://csound.com/). Kairos is made by [Leo Foletto](/member/leo.html).
